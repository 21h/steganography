'''
Copyright by Vladimir Smagin
http://blindage.org
21h@blindage.org
'''

from PIL import Image, ImageFilter

im = Image.open( 'input.png' )
im = im.convert("RGB")
x, y = im.size

#ask user
myHiddenText = input("Enter message: ")

#prepare for battle!
textSize = len(myHiddenText)
textCurrentSymPos = 0
textCurrentBitPos = 0
newPixelColor = tuple()

bitsNumber = textSize*8
availableBits = x * y * 3

if bitsNumber > availableBits:
	print("Message too long for this image. Use bigger image or shorter message.")
	exit(1)

for coord_y in range(0,y):
	for coord_x in range(0,x):
		#get original pixel colors
		pixelColor = im.getpixel((coord_x, coord_y))
		print("\nPixel {} {}: COLOR: {}".format(coord_x,coord_y,pixelColor))
		#shift bits in pixel colors
		for color in pixelColor:
			if textCurrentSymPos <= textSize-1:
				# a bit of magic
				if textCurrentBitPos <= 7:
					colorShift = (ord(myHiddenText[textCurrentSymPos]) << textCurrentBitPos) >> 7 & 1
					if colorShift:
						newPixelColor += ((color | colorShift),) #set 1
					else:
						newPixelColor += ((color &~ 1),) #set 0
					print("%s" % colorShift, end='')
					textCurrentBitPos += 1
				else:
					textCurrentBitPos = 0
					textCurrentSymPos += 1
					if textCurrentSymPos < textSize: 
						colorShift = (ord(myHiddenText[textCurrentSymPos]) << textCurrentBitPos) >> 7 & 1
						if colorShift:
							newPixelColor += ((color | colorShift),) #set 1
						else:
							newPixelColor += ((color &~ 1),) #set 0
						print("%s" % colorShift, end='')
						textCurrentBitPos += 1
					else:
						for k in range(len(newPixelColor),3):
							newPixelColor += (0,)
						print(newPixelColor)
				if textCurrentSymPos == textSize:
					pass
				else:
					print(": {} {} {}".format(ord(myHiddenText[textCurrentSymPos]), 
						myHiddenText[textCurrentSymPos], hex(ord(myHiddenText[textCurrentSymPos]))), end='\n')
			else:
				pass
		if len(newPixelColor)==3:
			im.putpixel((coord_x, coord_y), newPixelColor)
			newPixelColor = tuple()



im.save('output.png', 'PNG')
