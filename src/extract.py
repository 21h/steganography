'''
Copyright by Vladimir Smagin
http://blindage.org
21h@blindage.org
'''

from PIL import Image, ImageFilter

mask = 1
extractedBytes = list()

im = Image.open( 'output.png' )
im = im.convert("RGB")
x, y = im.size

#prepare for battle!
bitPosInByte = 7
newByte = 0

for coord_y in range(0,y):
	for coord_x in range(0,x):
		r, g, b = im.getpixel((coord_x, coord_y))
		bitcort=(int(mask & r),int(mask & g),int(mask & b))
		#print("X: {}, Y: {} BITS: {} {} {}".format(coord_x,coord_y,bitcort[0],bitcort[1],bitcort[2],))
		for bit in bitcort:
			if bitPosInByte <=0: 
				newByte |= bit << bitPosInByte
				extractedBytes.append(newByte)
				bitPosInByte = 7
				newByte = 0
			else:
				newByte |= bit << bitPosInByte
				bitPosInByte += -1


with open('test.bin', 'wb') as outfile:
	outfile.write(bytearray(extractedBytes))
